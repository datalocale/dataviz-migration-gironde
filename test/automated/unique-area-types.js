import {strict as assert} from 'assert';
import * as areaTypes from '../../public/script/AREA_TYPE.js'

const areaTypesEntries = Object.entries(areaTypes)

describe('Unique area type values', () => {

    areaTypesEntries.forEach(([name, value], index) => {
        const otherAreaTypesEntries = areaTypesEntries.slice(index + 1);

        for(const [otherName, otherValue] of otherAreaTypesEntries){
            describe(`Comparing ${name} value and ${otherName} value`, () => {
                it('should be different', () => {
                    assert.notEqual(value, otherValue);
                });
            });
        }
    })

});
