# Sources de données

## Contexte

Ce dossier contient les données sources utilisées

Certaines sont versionnées, d'autres pas à cause de leur volume

## Provenance

### Données de migration

- 2013 : https://www.insee.fr/fr/statistiques/2409519?sommaire=2409559
- 2014 : https://www.insee.fr/fr/statistiques/2866333?sommaire=2866354
- 2015 : https://www.insee.fr/fr/statistiques/3566042?sommaire=3558417

Téléchargement manuel

### Données de correspondance entre identifiant de commune et nom/département/région

https://insee.fr/fr/information/2028028


## Transformations

Le fichier de correspondance entre identifiant de commune et nom/département/région de l'année 2015 a été téléchargé manuellement, ouvert avec LibreOffice 6.0.7.3 sur Ubuntu et la première feuille (COM) a été enregistrée en CSV
Puis suppression des 4 premières lignes

