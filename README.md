# dataviz-migration-gironde

travail de mise en forme visuelle des données du fichiers MIG de l'INSEE

[Voir](Dataviz_migrations_résidentielles.pdf) pour le contexte de réalisation de ce projet

* Doit permettre de consulter les flux migratoires à destination du département de la Gironde.
* Doit permettre de consulter les flux migratoires internes à la Gironde.
* Doit permettre de détailler ces flux en fonctions d'axes d'analyse multiples.

# API pour récupérer les données sur datalocale

https://docs.ckan.org/en/2.6/api/index.html

## Développement

1. Installer git, Node.js (qui vient avec npm)
2.
```sh
git clone git@gitlab.com:datalocale/dataviz-migration-gironde.git
cd dataviz-migration-gironde

# installe les dépendances du projet
npm ci

# récupère les données INSEE (~600Mo)
npm run download-data

# Construire les fichiers utiles à partir des données brutes
npm start
```

Lance ensuite la commande `npx serve --listen 5000 public` puis ouvre [localhost:5000](http://localhost:5000) dans un navigateur web pour voir la dataviz.
