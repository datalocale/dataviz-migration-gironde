import {render} from 'https://unpkg.com/htm@2.0.0/preact/standalone.mjs'
import {createStore} from 'https://unpkg.com/redux@4.0.1/es/redux.mjs'

import SelectionSection from './components/SelectionSection.js'
import DetailsSection from './components/DetailsSection.js'

import LayeredMap from './components/LayeredMap.js'
import NonGirondePlaces from './components/NonGirondePlaces.js'

import BackgroundMap from './components/maps/gironde/departement.js'
import EPCIMap from './components/maps/gironde/epci.js';
import SCoTMap from './components/maps/gironde/scot.js';


import stateToDetailsData from './stateToDetailsData.js'

import migrationLeavingFromArriving from './migrationLeavingFromArriving.js'
import computeMigrationTotals from './computeMigrationTotals.js'

import {AREA_TYPE_DEPARTEMENT, AREA_TYPE_SCOT, AREA_TYPE_EPCI, AREA_TYPE_COMMUNE, AREA_TYPE_REGION, AREA_TYPE_NON_FRANCE_EU, AREA_TYPE_NON_FRANCE_NON_EU, AREA_TYPE_FRANCE_TOM} from './AREA_TYPE.js'
import {DIRECTION_LEAVING, DIRECTION_ARRIVING} from './DIRECTIONS.js'

import {AREA_CHANGE, DIRECTION_CHANGE, AREAS_DATA, MIGRATION_DATA, AREA_TYPE_CHANGE, DETAILS_TAB_CHANGE} from './ACTIONS.js'
import {DETAILS_TAB_AGE} from './DETAILS_TABS.js'


const DEPARTEMENT_AREA = {
    code: '33',
    name: 'Département de la Gironde',
    areaType: AREA_TYPE_DEPARTEMENT
}

function reducer(state, action){
    const {type} = action;

    switch(type){
        case AREA_CHANGE: {
            state.userSelected.area = action.area;
            state.userSelected.areaType = undefined;
            return state;
        }
        case DIRECTION_CHANGE: {
            state.userSelected.direction = action.direction;
            return state;
        }
        case AREAS_DATA: {
            state.areas = action.areas
            return state;
        }
        case MIGRATION_DATA: {
            const {migrations, areaType, direction} = action
            state.migrations[direction][areaType] = migrations;
            return state;
        }
        case AREA_TYPE_CHANGE: {
            state.userSelected.areaType = action.areaType;
            return state;
        }
        case DETAILS_TAB_CHANGE: {
            state.userSelected.detailsTab = action.tab;
            return state;
        }

        default: {
            console.warn('unknown type', type)
            return state;
        }
    }
}

const store = createStore(
    reducer,
    {
        userSelected : {
            area : DEPARTEMENT_AREA,
            direction: DIRECTION_ARRIVING,
            areaType: undefined,
            detailsTab: DETAILS_TAB_AGE
        },
        areas: undefined,
        migrations: {
            [DIRECTION_ARRIVING]: {
                [AREA_TYPE_COMMUNE]: undefined,
                [AREA_TYPE_EPCI]: undefined,
                [AREA_TYPE_SCOT]: undefined,
                [AREA_TYPE_DEPARTEMENT]: undefined
            },
            [DIRECTION_LEAVING]: {
                [AREA_TYPE_COMMUNE]: undefined,
                [AREA_TYPE_EPCI]: undefined,
                [AREA_TYPE_SCOT]: undefined,
                [AREA_TYPE_DEPARTEMENT]: undefined
            }
        }
    }
)

store.subscribe(() => {
    appRender(store.getState())
})

function appRender(state){
    const {direction, area, areaType} = state.userSelected

    const migrationTotals = state.migrations[DIRECTION_ARRIVING][state.userSelected.area.areaType] && 
        state.migrations[DIRECTION_LEAVING][state.userSelected.area.areaType] &&
        computeMigrationTotals(
            state.migrations[DIRECTION_ARRIVING][state.userSelected.area.areaType]
                .find(migr => migr.destination.code === area.code),
            
            state.migrations[DIRECTION_LEAVING][state.userSelected.area.areaType]
                .find(migr => migr.origin.code === area.code)
        )

    render(
        SelectionSection(
            {
                areas: state.areas || [],
                chosenArea: area,
                direction,
                migrationTotals,
                onAreaChosen(area){
                    store.dispatch({
                        type: AREA_CHANGE,
                        area
                    })
                },
                onDirectionChosen(direction){
                    store.dispatch({
                        type: DIRECTION_CHANGE,
                        direction
                    })
                }
            }
        ),
        document.body.querySelector(`.selection`)
    );

    if(stateToDetailsData(state)){
        render(
            DetailsSection(stateToDetailsData(state, store.dispatch)),
            document.body.querySelector(`.details`)
        )

        let migrationsCountsByAreaId;
        let migrationsCountsByRegionId;
        let migrationsCountsByAreaTypeId
        let totalMigrationsCount;

        if(area.areaType === AREA_TYPE_SCOT || area.areaType === AREA_TYPE_EPCI){
            const selectedMigrations = !state.migrations[direction][area.areaType] ? 
                undefined : 
                direction === DIRECTION_ARRIVING ?
                    state.migrations[direction][area.areaType].find(({destination}) => destination.code === area.code) : 
                    state.migrations[direction][area.areaType].find(({origin}) => origin.code === area.code);

            if(selectedMigrations){
                const migrationsCollection = Object.values(
                    direction === DIRECTION_ARRIVING ?
                        selectedMigrations.migrationsByOrigin :
                        selectedMigrations.migrationsByDestination 
                )
                
                migrationsCountsByAreaId = new Map()
                migrationsCountsByRegionId = new Map()
                migrationsCountsByAreaTypeId = new Map()
                totalMigrationsCount = 0;
                
                for(const {origin, destination, details} of migrationsCollection){
                    const other = origin || destination;

                    switch(other.areaType){
                        case AREA_TYPE_SCOT:
                        case AREA_TYPE_EPCI: {
                            for(const {IPONDI} of details){
                                migrationsCountsByAreaId.set(
                                    other.code,
                                    (migrationsCountsByAreaId.get(other.code) || 0) + IPONDI
                                )
                                totalMigrationsCount += IPONDI;
                            }
                            break;
                        }
                        case AREA_TYPE_REGION: {
                            for(const {IPONDI} of details){
                                migrationsCountsByRegionId.set(
                                    other.code,
                                    (migrationsCountsByRegionId.get(other.code) || 0) + IPONDI
                                )
                                totalMigrationsCount += IPONDI;
                            }
                            break;
                        }
                        case AREA_TYPE_NON_FRANCE_EU:
                        case AREA_TYPE_NON_FRANCE_NON_EU:
                        case AREA_TYPE_FRANCE_TOM: {
                            for(const {IPONDI} of details){
                                migrationsCountsByAreaTypeId.set(
                                    other.areaType,
                                    (migrationsCountsByAreaTypeId.get(other.areaType) || 0) + IPONDI
                                )
                                totalMigrationsCount += IPONDI;
                            }
                            break;
                        }

                        default: {
                            console.warn('migrations not counted. Area type', other.areaType)
                        }
                    }
                }
            }
        }

        render(
            LayeredMap({
                areas: state.areas || [],
                chosenArea: area,
                chosenAreaType: areaType,
                migrationsCountsByAreaId,
                totalMigrationsCount,
                BackgroundMap,
                LayerMaps: new Map([
                    [AREA_TYPE_SCOT, SCoTMap],
                    [AREA_TYPE_EPCI, EPCIMap],
                ]),
                areaTypes: [
                    AREA_TYPE_DEPARTEMENT,
                    AREA_TYPE_SCOT,
                    AREA_TYPE_EPCI
                ],
                onAreaTypeSelection(areaType) {
                    store.dispatch({
                        type: AREA_TYPE_CHANGE,
                        areaType
                    })
                },
                onAreaChosen(area){
                    store.dispatch({
                        type: AREA_CHANGE,
                        area
                    })
                }
            }),
            document.body.querySelector('.gironde')
        )


        render(
            NonGirondePlaces({
                migrationsCountsByRegionId,
                migrationsCountsByAreaTypeId,
                totalMigrationsCount,
                direction
            }),
            document.body.querySelector(`.non-gironde`)
        )
    }


}

appRender(store.getState());


// Initial data fetch and app bootstrap

const migrCommunesP = d3.json('./data/migrations-communes-gironde.json');
migrCommunesP
.then(migrations => {
    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_COMMUNE,
        direction: DIRECTION_ARRIVING,
        migrations
    })

    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_COMMUNE,
        direction: DIRECTION_LEAVING,
        migrations: migrationLeavingFromArriving(migrations)
    })

})

d3.json('./data/migrations-EPCI-gironde.json')
.then(migrations => {
    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_EPCI,
        direction: DIRECTION_ARRIVING,
        migrations
    })

    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_EPCI,
        direction: DIRECTION_LEAVING,
        migrations: migrationLeavingFromArriving(migrations)
    })
});

d3.json('./data/migrations-SCoT-gironde.json')
.then(migrations => {
    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_SCOT,
        direction: DIRECTION_ARRIVING,
        migrations
    })

    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_SCOT,
        direction: DIRECTION_LEAVING,
        migrations: migrationLeavingFromArriving(migrations)
    })
})

d3.json('./data/migrations-département-gironde.json')
.then(migrations => {
    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_DEPARTEMENT,
        direction: DIRECTION_ARRIVING,
        migrations
    })

    store.dispatch({
        type: MIGRATION_DATA,
        areaType: AREA_TYPE_DEPARTEMENT,
        direction: DIRECTION_LEAVING,
        migrations: migrationLeavingFromArriving(migrations)
    })
})


const communeByCodeP = Promise.all([
    d3.csv('./data/table-appartenance-geo-communes-15.csv')
        .then(communes => {
            const communeByCode = new Map()
            for(const commune of communes){
                communeByCode.set(commune['Code géographique'], commune)
            }
            return communeByCode;
        }),
    migrCommunesP
])
.then(([allCommuneByCode, migrCommunes]) => {
    const communeByCode = new Map()

    for(const {destination} of migrCommunes){
        if(allCommuneByCode.has(destination.code)){
            const commune = allCommuneByCode.get(destination.code)
            // making commune fit to Area interface
            commune.name = commune['Libellé géographique']
            commune.code = commune['Code géographique']
            commune.areaType = AREA_TYPE_COMMUNE;

            communeByCode.set(destination.code, commune)
        }
    }

    return communeByCode;
})

const correspP = d3.csv('./data/tableau_correspondances.csv')
    .then(correspondances => {
        const EPCIByCode = new Map()
        const SCoTByCode = new Map()

        for(const c of correspondances){
            EPCIByCode.set(c['CODE_EPCI'], {
                code: c['CODE_EPCI'],
                name: c['EPCI\n'].replace(/\n/g, ' '),
                areaType: AREA_TYPE_EPCI
            })
            SCoTByCode.set(c['CODE_SCOT'], {
                code: c['CODE_SCOT'],
                name: c['SCoT\n'].replace(/\n/g, ' '),
                areaType: AREA_TYPE_SCOT
            })
        }

        return {EPCIByCode, SCoTByCode};
    })



Promise.all([communeByCodeP, correspP])
.then(([communeByCode, {EPCIByCode, SCoTByCode}]) => {
    console.log('EPCIByCode', EPCIByCode)


    store.dispatch({
        type: AREAS_DATA,
        areas: [
            DEPARTEMENT_AREA,
            ...communeByCode.values(),
            ...EPCIByCode.values(),
            ...SCoTByCode.values()
        ]
    })

})
.catch(console.error)
