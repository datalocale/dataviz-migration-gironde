export default function(migrationArriving){
    const migrationLeaving = []

    for(const {destination, migrationsByOrigin} of migrationArriving){
        for(const {origin, details} of migrationsByOrigin){
            const existingMigrationLeaving = migrationLeaving.find(m => m.origin.code === origin.code)

            const migrationsByOrigin = existingMigrationLeaving || {
                origin, migrationsByDestination: []
            }

            const {migrationsByDestination} = migrationsByOrigin
            migrationsByDestination.push({ destination, details })

            if(!existingMigrationLeaving){
                migrationLeaving.push(migrationsByOrigin)
            }
        }
    }

    return migrationLeaving
}