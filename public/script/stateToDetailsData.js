import {DIRECTION_ARRIVING} from './DIRECTIONS.js'
import {DETAILS_TAB_CHANGE} from './ACTIONS.js'

import * as tabs from './DETAILS_TABS.js'

export default function(state, dispatch){
    const {userSelected, migrations} = state;
    const {area, direction} = userSelected
    const selectedMigrations = !migrations[direction][area.areaType] ? 
        undefined : 
        direction === DIRECTION_ARRIVING ?
            migrations[direction][area.areaType].find(({destination}) => destination.code === area.code) : 
            migrations[direction][area.areaType].find(({origin}) => origin.code === area.code);

    if(selectedMigrations){
        const migrationsCollection = Object.values(
            direction === DIRECTION_ARRIVING ?
                selectedMigrations.migrationsByOrigin :
                selectedMigrations.migrationsByDestination 
        )

        const migrationsByAgeCategory = new Map();
        const migrationsByCS1 = new Map();
        const migrationsByNPERR = new Map(); 
        const migrationsByTACT = new Map(); 

        for(const {details} of migrationsCollection){
            for(const {AGEREVQ, CS1, NPERR, TACT, IPONDI} of details){
                migrationsByAgeCategory.set(
                    AGEREVQ,
                    (migrationsByAgeCategory.get(AGEREVQ) || 0) + IPONDI
                )
                migrationsByCS1.set(
                    CS1,
                    (migrationsByCS1.get(CS1) || 0) + IPONDI
                )
                migrationsByNPERR.set(
                    NPERR,
                    (migrationsByNPERR.get(NPERR) || 0) + IPONDI
                )
                migrationsByTACT.set(
                    TACT,
                    (migrationsByTACT.get(TACT) || 0) + IPONDI
                )
            }
        }

        return {
            tabs: Object.values(tabs),
            selectedTab: state.userSelected.detailsTab,
            migrationsByAgeCategory,
            migrationsByCS1,
            migrationsByNPERR,
            migrationsByTACT,
            dispatchTabChange(tab){
                dispatch({
                    type: DETAILS_TAB_CHANGE,
                    tab
                })
            }
        }
    }
}