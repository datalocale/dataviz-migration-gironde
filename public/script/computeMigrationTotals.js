import {DIRECTION_LEAVING, DIRECTION_ARRIVING} from './DIRECTIONS.js'

export default function(
    arrivingMigrations,
    leavingMigrations
){
    let arrivings = 0;

    for(const migrs of arrivingMigrations.migrationsByOrigin){
        for(const {IPONDI} of migrs.details)
            arrivings += IPONDI
    }

    let leavings = 0;

    for(const migrs of leavingMigrations.migrationsByDestination){
        for(const {IPONDI} of migrs.details)
            leavings += IPONDI
    }

    return {
        [DIRECTION_LEAVING]: Math.round(leavings),
        [DIRECTION_ARRIVING]: Math.round(arrivings)
    }

}