import {html} from 'https://unpkg.com/htm@2.0.0/preact/standalone.mjs'

import {DIRECTION_LEAVING, DIRECTION_ARRIVING} from '../DIRECTIONS.js'


/*
    areas is an array of Area

    interface Area{
        code: 'string' // unique identifier
        name: 'string' // human-relatable name
        areaType: AREA_TYPE
    }

*/
export default function AreaChoiceInput({areas, chosenArea, direction, migrationTotals, onAreaChosen}){
    const listId = 'area-list-'+(Math.random().toString(36).slice(2))

    return html`
        <div>
            <input list="${listId}" value="${chosenArea.name}" onInput=${e => {
                const area = areas.find(a => e.target.value === a.name)
                if(area)
                    onAreaChosen(area)
            }}/>
            ${migrationTotals ?
                html`
                    <div>
                        <p class=${direction === DIRECTION_ARRIVING ? '': 'discrete'}>
                            <strong>${migrationTotals[DIRECTION_ARRIVING]}</strong> personnes y ont emmenagé
                        </p>
                        <p class=${direction === DIRECTION_LEAVING ? '': 'discrete'}>
                            <strong>${migrationTotals[DIRECTION_LEAVING]}</strong> personnes en ont démenagé
                        </p>
                    </div>
                ` :
                undefined
            }

            <datalist id="${listId}">
                ${areas.map(a => {
                    return html`<option value="${a.name}"/>`
                })}
            </datalist>
        </div>
    `
}
