import {html} from 'https://unpkg.com/htm@2.0.0/preact/standalone.mjs'
import {DETAILS_TAB_AGE, DETAILS_TAB_CSP, DETAILS_TAB_FOYER, DETAILS_TAB_ACTIVITE} from '../DETAILS_TABS.js'


const INSEE_AGEREVQ_AGE_GROUPS = [
    {
        name: '0 - 15',
        values: ['000', '005', '010']
    },
    {
        name: '15 - 19',
        values: ['015']
    },
    {
        name: '20 - 24',
        values: ['020']
    },
    {
        name: '25 - 39',
        values: ['025', '030', '035']
    },
    {
        name: '40 - 54',
        values: ['040', '045', '050']
    },
    {
        name: '55-64',
        values: ['055', '060']
    },
    {
        name: '65-79',
        values: ['065', '070', '075']
    },
    {
        name: '80+',
        values: ['080', '085', '090', '095', '100', '105', '110', '115', '120']
    }
]

const CS1Names = new Map([
    ['1', "Agriculteurs exploitants"],
    ['2', "Artisans, commerçants et chefs d'entreprise"],
    ['3', "Cadres et professions intellectuelles supérieures"],
    ['4', "Professions Intermédiaires"],
    ['5', "Employés"],
    ['6', "Ouvriers"],
    ['7', "Retraités"],
    ['8', "Autres personnes sans activité professionnelle"]
])

const TACTNames = new Map([
    ['11', "Actifs ayant un emploi, y compris sous apprentissage ou en stage rémunéré"],
    ['12', "Chômeurs"],
    ['21', "Retraités ou préretraités"],
    ['22', "Elèves, étudiants, stagiaires non rémunéré de 14 ans ou plus"],
    ['23', "Moins de 14 ans"],
    ['24', "Femmes ou hommes au foyer"],
    ['25', "Autres inactifs"]
])


function AgeRepresentation({countByAgeCategory}){
    let sum = 0; for(const count of countByAgeCategory.values()){ sum += count }

    return html`
        <div class="detail-content age-representation">
            <h2>Âge</h2>
            <ul>
                ${[...countByAgeCategory.entries()].map(([category, count]) => {
                    // work around https://github.com/developit/htm/issues/60
                    const styleStr = `width: ${(count/sum)*100}%;`

                    return html`
                        <li> 
                            <span>${category}</span>
                            <div style="${styleStr}" class="bar"></div>
                        </li>
                    `
                })}
            </ul>
        </div>
    `
}

function CS1Representation({migrationsByCS1}){
    let sum = 0; for(const count of migrationsByCS1.values()){ sum += count }

    return html`
        <div class="detail-content cs1-representation">
            <h2>Catégories socio-professionnelle</h2>
            <ul>
                ${[...migrationsByCS1.entries()].map(([category, count]) => {
                    // work around https://github.com/developit/htm/issues/60
                    const styleStr = `width: ${(count/sum)*100}%;`

                    return html`
                        <li> 
                            <span>${CS1Names.get(category)}</span>
                            <div style="${styleStr}" class="bar"></div>
                        </li>
                    `
                })}
            </ul>
        </div>
    `
}


function FoyerRepresentation({migrationsByNPERR}){
    let sum = 0; for(const count of migrationsByNPERR.values()){ sum += count }

    return html`
        <div class="detail-content foyer-representation">
            <h2>Taille du foyer</h2>
            <ul>
                ${[...migrationsByNPERR.entries()].map(([number, count]) => {
                    // work around https://github.com/developit/htm/issues/60
                    const styleStr = `width: ${(count/sum)*100}%;`

                    return html`
                        <li> 
                            <span>${number} personnes</span>
                            <div style="${styleStr}" class="bar"></div>
                        </li>
                    `
                })}
            </ul>
        </div>
    `
}

function TACTRepresentation({migrationsByTACT}){
    let sum = 0; for(const count of migrationsByTACT.values()){ sum += count }

    return html`
        <div class="detail-content tact-representation">
            <h2>Taille du foyer</h2>
            <ul>
                ${[...migrationsByTACT.entries()].map(([category, count]) => {
                    // work around https://github.com/developit/htm/issues/60
                    const styleStr = `width: ${(count/sum)*100}%;`

                    return html`
                        <li> 
                            <span>${TACTNames.get(category)}</span>
                            <div style="${styleStr}" class="bar"></div>
                        </li>
                    `
                })}
            </ul>
        </div>
    `
}


function makeTabContent({selectedTab, migrationsByAgeCategory, migrationsByNPERR, migrationsByTACT, migrationsByCS1}){
    switch(selectedTab){
        case DETAILS_TAB_AGE: {
            const countByAgeCategory = new Map()

            for(const {name, values} of INSEE_AGEREVQ_AGE_GROUPS){
                let count = 0;
                for(const v of values){
                    count += (migrationsByAgeCategory.get(v) || 0)
                }
                countByAgeCategory.set(name, Math.round(count))
            }

            return html`<${AgeRepresentation} countByAgeCategory=${countByAgeCategory}/>`
        }

        case DETAILS_TAB_CSP: {
            return html`<${CS1Representation} migrationsByCS1=${migrationsByCS1}/>`
        }

        case DETAILS_TAB_FOYER: {
            return html`<${FoyerRepresentation} migrationsByNPERR=${migrationsByNPERR}/>`
        }

        case DETAILS_TAB_ACTIVITE: {
            return html`<${TACTRepresentation} migrationsByTACT=${migrationsByTACT}/>`
        }

        default: {
            console.error('unknown tab', selectedTab)
            return;
        }

    }
}


export default function DetailsSection({migrationsByAgeCategory, migrationsByCS1, migrationsByNPERR, migrationsByTACT, tabs, selectedTab, dispatchTabChange}){
    const content = makeTabContent({migrationsByAgeCategory, migrationsByCS1, migrationsByNPERR, migrationsByTACT, selectedTab})

    return html`
        <div>
            ${content}
            <ul class="tabs">
                ${tabs.map(tab => {
                    return html`<li 
                        class="${tab === selectedTab ? 'active': undefined}"
                        onClick="${e => dispatchTabChange(tab)}"
                        >
                        ${tab.slice('DETAILS_TAB_'.length)}
                        </li>`
                })}

            </ul>
        </div>
    `
}