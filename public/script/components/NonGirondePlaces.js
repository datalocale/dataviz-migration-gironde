import {html} from 'https://unpkg.com/htm@2.0.0/preact/standalone.mjs'

import FranceMap from './maps/france.js'

import {AREA_TYPE_NON_FRANCE_EU, AREA_TYPE_NON_FRANCE_NON_EU, AREA_TYPE_FRANCE_TOM} from '../AREA_TYPE.js'
import {DIRECTION_LEAVING} from '../DIRECTIONS.js'


// pass all proportions 
export default function NonGirondePlaces({migrationsCountsByRegionId, migrationsCountsByAreaTypeId, totalMigrationsCount: total, direction}){

    const otherData = migrationsCountsByAreaTypeId ? {
        [AREA_TYPE_FRANCE_TOM]: {
            count: migrationsCountsByAreaTypeId.get(AREA_TYPE_FRANCE_TOM),
            style: `background-color: hsl(120, 70%, ${
                ((1 - migrationsCountsByAreaTypeId.get(AREA_TYPE_FRANCE_TOM) / total) * 100).toFixed(1)
            }%);`,
            text: 'TOM'
        },
        [AREA_TYPE_NON_FRANCE_EU]: {
            count: migrationsCountsByAreaTypeId.get(AREA_TYPE_NON_FRANCE_EU),
            style: `background-color: hsl(120, 70%, ${
                ((1 - migrationsCountsByAreaTypeId.get(AREA_TYPE_NON_FRANCE_EU) / total) * 100).toFixed(1)
            }%);`,
            text: 'UE'
        },
        [AREA_TYPE_NON_FRANCE_NON_EU]: {
            count: migrationsCountsByAreaTypeId.get(AREA_TYPE_NON_FRANCE_NON_EU),
            style: `background-color: hsl(120, 70%, ${
                ((1 - migrationsCountsByAreaTypeId.get(AREA_TYPE_NON_FRANCE_NON_EU) / total) * 100).toFixed(1)
            }%);`,
            text: 'hors UE'
        }
    } : []

    return html`
        <div>
            <${FranceMap} migrationsCountsByRegionId=${migrationsCountsByRegionId} totalMigrationsCount=${total}/>

            <ul class="other">
                ${
                    [AREA_TYPE_FRANCE_TOM, AREA_TYPE_NON_FRANCE_EU, AREA_TYPE_NON_FRANCE_NON_EU]
                    .map(a => otherData[a] || {})
                    .map(({style, text}) => {
                        return html`
                            <li>
                                <span class="${direction === DIRECTION_LEAVING ? 'no-data' : 'color'}" style=${style}></span>
                                <span>${text}</span>
                            </li>`
                    })
                }
            </ul>
        </div>
    `
}
