import {html} from 'https://unpkg.com/htm@2.0.0/preact/standalone.mjs'

import AreaChoiceInput from './AreaChoiceInput.js'
import DirectionToggle from './DirectionToggle.js'

export default function({ areas, chosenArea, migrationTotals, direction, onAreaChosen, onDirectionChosen }){
    return html`
        <div>
            <${AreaChoiceInput} areas=${areas} chosenArea=${chosenArea} migrationTotals=${migrationTotals} direction=${direction} onAreaChosen=${onAreaChosen}/>
            <${DirectionToggle} direction=${direction} onDirectionChosen=${onDirectionChosen}/>
        </div>
    `
}
