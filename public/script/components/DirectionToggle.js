import {html} from 'https://unpkg.com/htm@2.0.0/preact/standalone.mjs'
import * as directions from '../DIRECTIONS.js'

export default function AreaChoiceInput({direction, onDirectionChosen}){
    return html`
        <div>
            ${
                Object.values(directions).map(d => {
                    return html`
                        <label>${d}
                            <input 
                                type="radio" 
                                name="direction" 
                                checked="${d === direction}"
                                onChange=${e => { onDirectionChosen(d) }}
                            />
                        </label>
                    `
                })
            }            
        </div>                
    `
}