import {html} from 'https://unpkg.com/htm@2.0.0/preact/standalone.mjs'
import cx from 'https://unpkg.com/clsx@1.0.2/dist/clsx.mjs'

function makeSCoTStyles(migrationsCountsByAreaId, total){

  return [...migrationsCountsByAreaId.entries()]
    .map(([id, count]) => {
      return `[id="${id}"]{
        fill: hsl(120, 70%, ${((1-count/total)*100).toFixed(1)}%);
      }`
    })
    .join('\n\n')

}


export default function({BackgroundMap, LayerMaps=new Map(), migrationsCountsByAreaId, totalMigrationsCount, chosenArea, areas, chosenAreaType: areaType, areaTypes, onAreaChosen, onAreaTypeSelection}) {
    
    const onAreaTypeClick = (event, areaType) => {
      event.preventDefault()
      onAreaTypeSelection(areaType)
    }

    const chosenAreaCode = chosenArea ? chosenArea.code : undefined;
    const chosenAreaType = areaType || chosenArea.areaType;

    const onAreaClick = ({target}) => {
      const area = areas.find(a => a.code === target.id && a.areaType === chosenAreaType)
      if(area){
        onAreaChosen(area)
      }
      else{
        console.error('No area with id:', target.id, 'area type:', chosenAreaType)
      }
    }


    // complete migration counts with visualized SCoTs that have no migrations
    if(migrationsCountsByAreaId){
      for(const el of document.querySelectorAll('g#SCoT polygon[id]')){
        if(!migrationsCountsByAreaId.has(el.id))
          migrationsCountsByAreaId.set(el.id, 0)
      }
    }

    return html`
      <section class="layered-maps">
        <style>
        [id="${chosenAreaCode}"]{
          fill: green;
          stroke-width: 3px;
        }
        
        ${migrationsCountsByAreaId ? makeSCoTStyles(migrationsCountsByAreaId, totalMigrationsCount) : ''}
        </style>

        <div class="layers">
          ${[...LayerMaps.entries()].map(([areaType, map]) => html`
            <div class="${cx('layers-layer', (chosenAreaType === areaType) && 'active')}" onClick="${onAreaClick}" onKeyDown="${e => e.keyCode === 13 && onAreaClick(e)}"><${map} /></div>
          `)}
        </div>

        <div class="background-map">
          <${BackgroundMap} />
        </div>

        <nav class="area-type-selection">
          <ul class="area-type-selection-list">
            ${areaTypes.map(areaType => html`
              <li>
                <a href="${'#' + areaType}" class="${cx({active: areaType === chosenAreaType})}" onClick="${e => onAreaTypeClick(e, areaType)}">${areaType}</a>
              </li>
            `)}
          </ul>
        </nav>
      </section>
    `
}
