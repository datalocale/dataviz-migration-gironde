## atelier le 8/01/2019

Rodolphe et Pascal

### scenarii d'usage

#### page d'accueil

Plusieurs volets permettent de choisir entre la réponse à la question : ça démennage en Gironde ! et la question Et demain ?

> Rq : on pourrait imaginer par la suite un troisième volet Et hier ? si on arrive à rajouter les données rpovenant de la méthodologie de calcul précédente qui a pour particularité d'avoir plusieurs pas de temps plus importants

Réfléchir à un effet visuel d'animation qui introduirait les donnéés globales de migration en Gironde en présentant les 5,6 sources géographiques principales d'immigration et les répartirait sur les 5 scots de Gironde

A la fin de l'animation la carte présente le chiffre de population intégrant cette quantité de migrants ainsi que le pourcentage d'augementation ou de diminution

![animation](../../images/animationSeingDataUk.png)

### Page Et demain ?

Présentation des trois scenari de prospectives hypothèse basse, médiane et haute
Pas de possibilité de disposer des crtières de filtres car il ne s'agit que de données d'ordre général.

> Rq : A moins que l'on s'amuse à les calculer nous même à partir des données globales fournies

### Page ça déménage

En haut de la page se situe le critère de choix par année : on départ 3 années sont disponibles et au bout de 2 ans on aura 5 choix
Si on veut on peut avoir un choix à l'extrême droite qui est l'année du futur 2033 par exemple
Si on décide d'inclure les données construites suivant la méthodologie précédente on a des pas de temps de 3 ans (200--2009 / 2010-2013, etc..)

On présente la Gironde en carte stylisée avec le nom des 5 scots
A l'intérieur figurent les points symbolisant les cdc (communautés de communes)
Si on clique sur une cdc on obtient son solde migratoire et les points des communes associées apparaissent

La carte initiale présente le solde migratoire pour le département et à l'intérieur de chaque scot le solde particulier

Trois zones de sélection au dessus de la carte permettent de faire de l'auto-complétion ou de choisir parmi une liste des termes : les scots, les cdc, les communes

Si on sélectionne un scot alors seuls les cdc et les communes en faisant partie restent disponibles dans les champs de sélection

A droite de l'écran se trouvent des curseurs qui vont s'appliquer sur le ou les territoires sélectionnés sur l'écran principal

- curseur activité pro : une liste de 5, 6 types
- curseur âge (doit permettre de choisir entre tel âge et tel âge avec des pas de 5 ou 10 ans)
- curseur type famille
- curseur sexe

Si on finit par choisir un territoire sa fiche descriptive de migration s'affiche
Elle contient plusieurs onglets et un classement indiquant son score au sein de l'ensemble des périmètres de regroupement (scot, cdc ou commune) disponibles
Elle affiche la population, le taux d'augmentation ou de réduction de la migration

- un onglet pour les professions
- un onglet pour les âges
- un onglet pour la composition familiale
- un onglet pour les tops origines ou la composition des origines (23% hors UE, 34% UE, 17% France, 45% Gironde)

A ce stade il est possible de choisir un autre teritoire comparable et de les afficher conjointement

A plusieurs endroit on peut aller vers la rubrique Et demain si les données sont disponibles à ce niveau de sélection

Ces idées de navigation et d'interaction sont inspirées du projet [seing data Uk](http://seeingdata.cleverfranke.com/census/#)

![maquette](../../images/seeingDataUk.PNG)
