# Projet migrations - 13 décembre 2018

**Présents** : 

- Rodolphe Zuniga (Service Coopération Territoriale)
- Pascal Romain (DSIN)
- David Bruant (dtc innovation)

## Contexte

Pascal Romain : 

Discussion en interne sur l'outillage pour la dataviz (Dig Dash ou spécifique) : open source

Contraintes annoncées :
- Commande de la VP Christine Bost pour montrer le produit fini
    - 20 février (Solutions Solidaires au Pin Galant)
    - début janvier pour le tour des élus
- "effet Whaou"

Powerpoint sur le gitlab a mis la barre haute pour montrer que DigDash ne peut pas répondre
On va être obligé de respecter certaines de ces contraintes

Outils pour les techniciens et élus

Migrations résidentielles (déménagement)
(+20000 girondins par an)

Equipements : financer les communes/intercommunalités (subventions) ou CD33 en tant que MOA

SCOT : document d'urbanisme de planification territoriale qui donne des grandes orientations
"on veut des zones commerciales dans tel ou tel endroit du territoire"
"on veut des zones résidentielles dans tel ou tel endroit du territoire"
Puis les communes font des documents techniques (PLU,...)

Il existe un interSCOT sur le territoire départemental que coordonne le CD33

Des données de l'INSEE existent, mais trop volumineuses, galère à les utiliser sur un tableur numérique

PR : "On a rencontré l'INSEE"
Ils veulent valoriser leurs données
Faire des outils pour valoriser leurs données (documentation, réutilisation)

Contrainte en terme d'accueil : ce sont des vieilles données
Un fichier va sortir chaque année, mais avec 3 ans de retard (méthodologie INSEE)

Prospective : "si on continue de la même manière, quelles seront les conséquences sur X ?"

Rodolphe :
"On aimerait bien bientôt réunir les parties-prenantes (élus, inter-SCOT, etc.) pour un atelier ouvert"

Groupe de béta-testeurs à composer : 
- techniciens inter-SCOT
- l'agence d'urbanisme (financé par Métro, CRNA, CD33)
- des gens de l'Etat

Avoir un premier rendu le 20 février (Solutions Solidaires au Pin Galant)

En 2017-2018, les docs de planification utilisent des données de 2011, 
donc utiliser les données de 2013-14-15 va déjà avoir une valeur ajoutée.

"Aujourd'hui, il faut un collègue techos qui sait ouvrir une base de données pour exploiter les données de l'INSEE"

Proposition d'avoir un "arbitre" qui permet de garantir le respect des règles du jeu : Scrum master

Discussions sur Scrum, sur qui est PO, etc.
Discussions sur le fait de "résoudre des problèmes" contre "répondre à un besoin"

Exemple de besoin : je veux facilement croiser des indicateurs et des temporalités sur un territoire donné

Exemple de problème : je ne peux pas naviguer dans, ni interroger, les données sans l'aide d'un technicien

## Planning

**Prochaine réunion le 17/12 à 14h** 

Objectif : constitution du backlog

Rétro du sprint liminaire : le 17/12 à 11h30

Réunion de planification du sprint : date à définir

Démarrage du premier sprint de développement le 07/01/2019