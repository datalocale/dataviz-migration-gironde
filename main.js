import fs from 'fs'
import util from 'util'

import Parser from 'node-dbf';
import * as dsv from 'd3-dsv';

import {AREA_TYPE_UNKNOWN_COMMUNE, AREA_TYPE_UNKNOWN_REGION, AREA_TYPE_NON_FRANCE_EU, AREA_TYPE_NON_FRANCE_NON_EU, AREA_TYPE_REGION, AREA_TYPE_DEPARTEMENT, AREA_TYPE_SCOT, AREA_TYPE_EPCI, AREA_TYPE_COMMUNE, AREA_TYPE_FRANCE_TOM} from './public/script/AREA_TYPE.js'

const {csvParse} = dsv;
const {promisify} = util;

const GIRONDE_DEPARTEMENT_CODE = '33'
const FOCUSED_DEPARTEMENT_CODE = GIRONDE_DEPARTEMENT_CODE;

// Source : https://www.insee.fr/fr/statistiques/fichier/2409519/contenu_rp2013_migcom.pdf
const INSEE_NON_FRANCE_AREA_CODE = '99999';

const INSEE_MIGCOM_IRAN_NON_FRANCE_EU = '8';

const INPUT_COMMUNE_DATA_FILE_PATH = 'public/data/table-appartenance-geo-communes-15.csv';
const INPUT_COMMUNE_DATA_ARRONDISSEMENTS_FILE_PATH = 'public/data/table-appartenance-geo-communes-15-ARM.csv';
const INPUT_MIGRATION_FILE_PATH = 'data/RP2015_MIGCOM_dbase/FD_MIGCOM_2015.dbf';
const INPUT_COMMUNE_EPCI_SCOP_FILE_PATH = 'public/data/tableau_correspondances.csv';

const OUTPUT_COMMUNE_MIGRATION_FILE_PATH = 'public/data/migrations-communes-gironde.json';
const OUTPUT_EPCI_MIGRATION_FILE_PATH = 'public/data/migrations-EPCI-gironde.json';
const OUTPUT_SCOT_MIGRATION_FILE_PATH = 'public/data/migrations-SCoT-gironde.json';
const OUTPUT_DEPARTEMENT_MIGRATION_FILE_PATH = 'public/data/migrations-département-gironde.json';

// https://www.insee.fr/fr/information/2028040
const TOM_COMMUNES_CODES = new Set([
    '97501', '97502', '97701', '97801', '98411', '98412', '98413', '98414', '98415', '98611', '98612', '98613', '98711', '98712', '98713', '98714', '98715', '98716', '98717', '98718', '98719', '98720', '98721', '98722', '98723', '98724', '98725', '98726', '98727', '98728', '98729', '98730', '98731', '98732', '98733', '98734', '98735', '98736', '98737', '98738', '98739', '98740', '98741', '98742', '98743', '98744', '98745', '98746', '98747', '98748', '98749', '98750', '98751', '98752', '98753', '98754', '98755', '98756', '98757', '98758', '98801', '98802', '98803', '98804', '98805', '98806', '98807', '98808', '98809', '98810', '98811', '98812', '98813', '98814', '98815', '98816', '98817', '98818', '98819', '98820', '98821', '98822', '98823', '98824', '98825', '98826', '98827', '98828', '98829', '98830', '98831', '98832', '98833', '98901'
])

const DEPARTEMENT_AREA = {
    code: '33', 
    name: 'Département de la Gironde',
    areaType: AREA_TYPE_DEPARTEMENT
}

const readFile = promisify(fs.readFile)



const correspP = readFile(INPUT_COMMUNE_DATA_FILE_PATH, 'utf-8')
.then(csvParse)
const corresp2P = readFile(INPUT_COMMUNE_DATA_ARRONDISSEMENTS_FILE_PATH, 'utf-8')
.then(csvParse)

const communeByCodeP = Promise.all([correspP, corresp2P])
.then(([communes, arrondissements]) => {
    const communeByCode = new Map()
    for(const commune of communes){
        communeByCode.set(commune['Code géographique'], commune)
    }
    for(const arr of arrondissements){
        communeByCode.set(arr['Code géographique'], arr)
    }
    return communeByCode;
});


const epciScotP = readFile(INPUT_COMMUNE_EPCI_SCOP_FILE_PATH, 'utf-8')
.then(csvParse)

const EPCIByCommuneCodeP = epciScotP
.then(correspondances => {
    const EPCIByCommuneCode = new Map()
    for(const corresp of correspondances){
        EPCIByCommuneCode.set(corresp['CODE_COMMUNE'], {
            code: corresp['CODE_EPCI'],
            areaType: AREA_TYPE_EPCI
        })
    }
    return EPCIByCommuneCode;
})

const SCoTByCommuneCodeP = epciScotP
.then(correspondances => {
    const SCoTByCommuneCode = new Map()
    for(const corresp of correspondances){
        SCoTByCommuneCode.set(corresp['CODE_COMMUNE'], {
            code: corresp['CODE_SCOT'],
            areaType: AREA_TYPE_SCOT
        })
    }
    return SCoTByCommuneCode;
})


const parser = new Parser(INPUT_MIGRATION_FILE_PATH);



{ // parsing lifecycle
    parser.on('start', () => {
        console.log('dBase file parsing has started');
    });
    
    parser.on('header', (h) => {
        console.log('dBase file header has been parsed', h);
    });

    let recordCount = 0;

    parser.on('record', (record) => {
        if(recordCount === 0)
            console.log(record)

        recordCount++
        if(recordCount % 50000 === 0)
            console.log(recordCount)
    })

    console.time('parse')

    parser.on('end', (p) => {
        console.log('Finished parsing the dBase file', recordCount);
        console.timeEnd('parse')
    }) 

    // to test code on a small sample of record
    /*setTimeout(() => {
        console.log('Stopping early to test the results')
        parser.emit('end')

        setTimeout(() => {
            console.log('STOP')
            process.exit()
        }, 2*1000)

    }, 10*1000)*/
}

/*
    Create data structure of the shape:
    {
        destination: {
            code
            type
        }
        migrationsByOrigin: [
            {
                origin: {
                    code
                    type
                }
                details: [
                    {
                        AGEREVQ, CS1, NPERR, TACT, IPONDI
                    },
                    {
                        AGEREVQ, CS1, NPERR, TACT, IPONDI
                    }
                ]
            }
        ]
    }
*/
function accumulateMigrationRecord(map, origin, destination, record){
    let destinationInfos = map.get(destination.code) || {
        destination: {code: destination.code, areaType: destination.areaType},
        migrationsByOrigin: []
    }
    
    const {migrationsByOrigin} = destinationInfos;
    
    // Repeated linear search. Might need to optimize
    const existingMigrationForCorrectOriginAndDestination = migrationsByOrigin.find(m => m.origin.code === origin.code)
    let migrationsForCorrectOriginAndDestination = existingMigrationForCorrectOriginAndDestination || {
        origin: {code: origin.code, areaType: origin.areaType},
        details: []
    };

    const {AGEREVQ, CS1, NPERR, TACT, IPONDI} = record

    const sameDetailsMigrationDescription = migrationsForCorrectOriginAndDestination.details.find((migr) => {
        return ['AGEREVQ', 'CS1', 'NPERR', 'TACT'].every(field => migr[field] === record[field])
    })

    if(sameDetailsMigrationDescription){
        sameDetailsMigrationDescription['IPONDI'] += IPONDI
    }
    else{
        migrationsForCorrectOriginAndDestination.details.push({AGEREVQ, CS1, NPERR, TACT, IPONDI});
    }

    if(!existingMigrationForCorrectOriginAndDestination){
        migrationsByOrigin.push(migrationsForCorrectOriginAndDestination)
    }

    map.set(destination.code, destinationInfos)
}

const discoveredUnknownCodes = new Set()
function warnAboutUnknownCode(code, variable){
    if(!discoveredUnknownCodes.has(code)){
        console.warn(`Unknown ${variable} code:`, code)
        discoveredUnknownCodes.add(code)
    }
}


Promise.all([communeByCodeP, EPCIByCommuneCodeP, SCoTByCommuneCodeP])
.then( ([communeByCode, EPCIByCommuneCode, SCoTByCommuneCode]) => {
    console.log('communeByCode, EPCIByCommuneCode, SCoTByCommuneCode are ready')

    const girondeCommuneMigrations = new Map();
    const girondeEPCIMigrations = new Map();
    const girondeSCoTMigrations = new Map();
    const girondeDepartementMigrations = new Map();

    parser.on('record', (record) => {
        const destinationCode = record['COMMUNE'];
        const originCode = record['DCRAN'];

        if (destinationCode === originCode) {
            return;
        }

        let destination;
        // destinationCode is never 99999 in the INSEE data
        const commune = communeByCode.get(destinationCode);
        if(commune){
            destination = {
                code: destinationCode,
                areaType: AREA_TYPE_COMMUNE,
                details: commune
            }
        }
        else{
            if(TOM_COMMUNES_CODES.has(destinationCode)){
                destination = {
                    code: destinationCode,
                    areaType: AREA_TYPE_FRANCE_TOM
                }
            }
            else{
                warnAboutUnknownCode(destinationCode, 'COMMUNE')
                destination = {
                    code: destinationCode,
                    areaType: AREA_TYPE_UNKNOWN_COMMUNE
                }
            }
        }
        
        let origin;
        if(originCode === INSEE_NON_FRANCE_AREA_CODE){
            const codeAndType = record['IRAN'] === INSEE_MIGCOM_IRAN_NON_FRANCE_EU ? 
                AREA_TYPE_NON_FRANCE_EU : 
                AREA_TYPE_NON_FRANCE_NON_EU

            origin = {
                code: codeAndType,
                areaType: codeAndType
            }
        }
        else{
            const commune = communeByCode.get(originCode);
            if(commune){
                origin = {
                    code: originCode,
                    areaType: AREA_TYPE_COMMUNE,
                    details: commune
                }
            }
            else{
                if(TOM_COMMUNES_CODES.has(originCode)){
                    origin = {
                        code: originCode,
                        areaType: AREA_TYPE_FRANCE_TOM
                    }
                }
                else{
                    warnAboutUnknownCode(originCode, 'DCRAN')
                    origin = {
                        code: originCode,
                        areaType: AREA_TYPE_UNKNOWN_COMMUNE
                    }
                }
            }
        }
        
        // Ignore migrations that are neither from or to the Département of focus
        if(destination.areaType === AREA_TYPE_COMMUNE && 
            destination.details['Département'] !== FOCUSED_DEPARTEMENT_CODE &&
            origin.areaType === AREA_TYPE_COMMUNE && 
            origin.details['Département'] !== FOCUSED_DEPARTEMENT_CODE
        ){
            return;
        }

        
        if(origin.areaType === AREA_TYPE_COMMUNE && 
            origin.details['Département'] !== FOCUSED_DEPARTEMENT_CODE
        ){
            const regionCode = origin.details['Région 2016'];
            origin = {
                code: regionCode,
                areaType: AREA_TYPE_REGION
            }
        }

        if(origin.areaType === AREA_TYPE_UNKNOWN_COMMUNE){
            origin = {
                code: AREA_TYPE_UNKNOWN_REGION,
                areaType: AREA_TYPE_UNKNOWN_REGION
            }
        }

        if(destination.areaType === AREA_TYPE_COMMUNE && 
            destination.details['Département'] !== FOCUSED_DEPARTEMENT_CODE
        ){
            const regionCode = destination.details['Région 2016'];
            destination = {
                code: regionCode,
                areaType: AREA_TYPE_REGION
            }
        }

        if(destination.areaType === AREA_TYPE_UNKNOWN_COMMUNE){
            destination = {
                code: AREA_TYPE_UNKNOWN_REGION,
                areaType: AREA_TYPE_UNKNOWN_REGION
            }
        }

        // communes
        accumulateMigrationRecord(girondeCommuneMigrations, origin, destination, record)


        // EPCI
        const destinationEPCI = destination.areaType ===  AREA_TYPE_COMMUNE ? 
            EPCIByCommuneCode.get(destination.code) : 
            destination
        let originEPCI = origin.areaType ===  AREA_TYPE_COMMUNE ?
            EPCIByCommuneCode.get(origin.code) : 
            origin
        

        if((destinationEPCI.areaType === AREA_TYPE_EPCI || 
            originEPCI.areaType === AREA_TYPE_EPCI) &&
            destinationEPCI.code !== originEPCI.code
        ){
            accumulateMigrationRecord(girondeEPCIMigrations, originEPCI, destinationEPCI, record)
        }

        
        // SCoT
        const destinationSCoT = destination.areaType ===  AREA_TYPE_COMMUNE ? 
            SCoTByCommuneCode.get(destination.code) : 
            destination
        let originSCoT = origin.areaType ===  AREA_TYPE_COMMUNE ?
            SCoTByCommuneCode.get(origin.code) : 
            origin
        
        if((destinationSCoT.areaType === AREA_TYPE_SCOT || 
            originSCoT.areaType === AREA_TYPE_SCOT) &&
            destinationSCoT.code !== originSCoT.code
        ){
            accumulateMigrationRecord(girondeSCoTMigrations, originSCoT, destinationSCoT, record)
        }

        
        // Département
        const destinationDepartement = destination.areaType ===  AREA_TYPE_COMMUNE ? 
            DEPARTEMENT_AREA : 
            destination
        let originDepartement = origin.areaType ===  AREA_TYPE_COMMUNE ?
            DEPARTEMENT_AREA : 
            origin
        
        if((destinationDepartement.areaType === AREA_TYPE_DEPARTEMENT || 
            originDepartement.areaType === AREA_TYPE_DEPARTEMENT) &&
            destinationDepartement.code !== originDepartement.code
        ){
            accumulateMigrationRecord(girondeDepartementMigrations, originDepartement, destinationDepartement, record)
        }

    });

    parser.on('end', (p) => {

        fs.writeFile(
            OUTPUT_COMMUNE_MIGRATION_FILE_PATH, 
            JSON.stringify([...girondeCommuneMigrations.values()], null, 2), 'utf8', 
            err => err && console.error(`error writing ${OUTPUT_COMMUNE_MIGRATION_FILE_PATH}`, err)
        )
        fs.writeFile(
            OUTPUT_EPCI_MIGRATION_FILE_PATH, 
            JSON.stringify([...girondeEPCIMigrations.values()], null, 2), 'utf8', 
            err => err && console.error(`error writing ${OUTPUT_EPCI_MIGRATION_FILE_PATH}`, err)
        )
        fs.writeFile(
            OUTPUT_SCOT_MIGRATION_FILE_PATH, 
            JSON.stringify([...girondeSCoTMigrations.values()], null, 2), 'utf8', 
            err => err && console.error(`error writing ${OUTPUT_SCOT_MIGRATION_FILE_PATH}`, err)
        )
        fs.writeFile(
            OUTPUT_DEPARTEMENT_MIGRATION_FILE_PATH, 
            JSON.stringify([...girondeDepartementMigrations.values()], null, 2), 'utf8', 
            err => err && console.error(`error writing ${OUTPUT_DEPARTEMENT_MIGRATION_FILE_PATH}`, err)
        )
    });

    parser.parse();

})

process.on('uncaughtException', (err) => {
    console.error('uncaught', err)
    process.exit(1)
})
